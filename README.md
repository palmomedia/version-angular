# palmomedia Version Angular
![pipeline](https://gitlab.com/palmomedia/version-angular/badges/master/pipeline.svg?style=flat)

[![Gitter](https://badges.gitter.im/palmomedia/community.svg)](https://gitter.im/palmomedia/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

Version 3 - angular Version der palmomedia.de / JAMStack Webseite mit WP als Backend.

## Background
Hier mehr zur Version 1 - VueJS
https://gitlab.com/palmomedia/version-vuejs/-/blob/master/README.md

## Development server
`ng serve`

## Build
`ng build`
Das Kompilat findet sich unter `dist/`. 
Das `--prod` flag ist entscheident - näheres dazu im Verleich unter: https://angular.palmomedia.de/blog/eine-webseite-drei-frameworks-part-angular-jamstack

### Config
Zum stehlen des "Blog Beispiels" braucht ihr nur den API Endpoint in ./src/global.js auszutauschen.
Näheres dazu ebenfalls in der VueJS Variante: 
https://gitlab.com/palmomedia/version-vuejs/-/blob/master/README.md
