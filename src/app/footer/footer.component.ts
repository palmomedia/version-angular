import { Component, OnInit } from '@angular/core';
import { faGoogle, faHtml5 } from '@fortawesome/free-brands-svg-icons';
import { faHeartbeat } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  faGoogle = faGoogle;
  faHeartbeat = faHeartbeat;
  faHtml5 = faHtml5;

  constructor() { }

  ngOnInit() {
  }

}
