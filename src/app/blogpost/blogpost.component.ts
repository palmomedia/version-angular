import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { GlobalVariable } from '../global';
import { Router, ActivatedRoute } from '@angular/router';

import { faTwitter, faXing, faFacebook, faWhatsapp } from '@fortawesome/free-brands-svg-icons';
import { faEnvelope, faBackward } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-blogpost',
  templateUrl: './blogpost.component.html',
  styleUrls: ['./blogpost.component.scss']
})
export class BlogpostComponent implements OnInit {
  private baseApiUrl = GlobalVariable.BASE_API_URL;

  post_title;
  post_content;
  post_name;
  author_image;
  display_name;

  show = false;
  activePage = '';
  faTwitter = faTwitter;
  faFacebook = faFacebook;
  faXing = faXing;
  faWhatsapp = faWhatsapp;
  faEnvelope = faEnvelope;
  faBackward = faBackward;

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { 
    router.events.subscribe((val) => {
      if(this.activePage != this.route.snapshot.paramMap.get('id')) {
        this.activePage = this.route.snapshot.paramMap.get('id');
        this.loadPage();
      }
    })
  }

  loadPage() {
    let id = this.route.snapshot.paramMap.get('id');
    let url = (id ? id : 'home');
    this.http.get<any>(this.baseApiUrl + 'post/' + url).subscribe(data => {     
      if(data != null && (typeof data === 'object') && data.status != 404) {
        this.post_title = data.post_title;
        this.post_content = data.post_content;

        this.post_content  = this.post_content.replace(/<p\s/g, '<p data-aos="fade-right" ');
        this.post_content  = this.post_content.replace(/<p>/g, '<p data-aos="fade-right">');
        this.post_content  = this.post_content.replace(/<div/g, '<div data-aos="fade-right" ');
        this.post_content  = this.post_content.replace(/<h1/g, '<h1 data-aos="fade-right" ');
        this.post_content  = this.post_content.replace(/<h2/g, '<h2 data-aos="fade-right" ');
        this.post_content  = this.post_content.replace(/<h3/g, '<h3 data-aos="fade-right" ');
        this.post_content  = this.post_content.replace(/<h4/g, '<h4 data-aos="fade-right" ');
        this.post_content  = this.post_content.replace(/<img/g, '<img data-aos="fade-right" ');
        this.post_content  = this.post_content.replace(/<pre\s/g, '<pre data-aos="fade-right" ');
        this.post_content  = this.post_content.replace(/<pre>/g, '<pre data-aos="fade-right">');
        console.log(this.post_content);
        
        document.title = data.post_name + " | palmomedia ";

        this.post_name = data.post_name;
        this.author_image = data.author_image;
        this.display_name = data.display_name;
        this.show = true;
      } else {
        this.router.navigate(['404']);
      }
    })
  }

  ngOnInit() {   
      this.loadPage();
  }

}
