import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router, ActivatedRoute } from '@angular/router';
import { faHome } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  breadcrumb = [];
  activePage = '';
  faHome = faHome;

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { 
    router.events.subscribe((val) => {
      if(this.activePage != this.router.url) {
        this.activePage = this.router.url;
        this.reRenderBreadcrumb();
      }
    })
  }

  reRenderBreadcrumb(): void {
    this.breadcrumb = [];
    this.router.url.split("/").forEach(element => {
    if(element !== "")
      this.breadcrumb.push({name: element.toLowerCase().replace(/-/g, " "), url: element});
    });
  }

  ngOnInit(): void {    
    this.reRenderBreadcrumb();
  }

}
