import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { GlobalVariable } from '../global';
import { Router, ActivatedRoute } from '@angular/router';

import { faTwitter, faXing, faFacebook } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  private baseApiUrl = GlobalVariable.BASE_API_URL;
  posts;
  show = false;
  faTwitter = faTwitter;
  faFacebook = faFacebook;
  faXing = faXing;
  
  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.http.get<any>(this.baseApiUrl + 'post/').subscribe(data => {     
      console.log(this.baseApiUrl + 'post/');
      if(data != null && (typeof data === 'object') && data.status != 404) {
        this.posts = data;
        this.show = true;
        document.title = "BLOG | palmomedia "; 
      } else {
        this.router.navigate(['404']);
      }
    })
  }

}
