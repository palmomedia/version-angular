import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageComponent } from './page/page.component';
import { BlogComponent } from './blog/blog.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { BlogpostComponent } from './blogpost/blogpost.component';

const routes: Routes = [
  {path: '', component: PageComponent},
  {path: '404', component: NotfoundComponent},
  {path: 'blog', component: BlogComponent},
  {path: 'blog/:id', component: BlogpostComponent},
  {path: ':id', component: PageComponent},
  {path: '**', redirectTo: '/404', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
