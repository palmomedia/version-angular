import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { GlobalVariable } from '../global';
import { Router } from '@angular/router';

import { faGlobe } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  private baseApiUrl = GlobalVariable.BASE_API_URL;
  public isMenuCollapsed = true;
  navigation = [];
  show = false;
  faGlobe = faGlobe;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {   
    this.http.get<any>(this.baseApiUrl + 'sitemap').subscribe(data => {      
      if(data != null && (typeof data === 'object') && data.status != 404) {
        this.navigation = data;
        this.show = true;
      } 
    })
  }

}
