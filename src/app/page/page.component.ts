import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { GlobalVariable } from '../global';
import { Router, ActivatedRoute } from '@angular/router';
import * as AOS from 'aos';

interface Page {
  title: string;
  content: string;
}

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})

export class PageComponent implements OnInit {
  private baseApiUrl = GlobalVariable.BASE_API_URL;

  post_title;
  post_content;
  show = false;
  activePage = '';

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { 
    router.events.subscribe((val) => {
      if(this.activePage != this.route.snapshot.paramMap.get('id')) {
        this.activePage = this.route.snapshot.paramMap.get('id');
        this.loadPage();
      }
    })
  }

  loadPage() {
    let id = this.route.snapshot.paramMap.get('id');
    let url = (id ? id : 'home');
    this.http.get<any>(this.baseApiUrl + 'page/' + url).subscribe(data => {     
      if(data != null && (typeof data === 'object') && data.status != 404) {
        this.post_title = data.post_title;
        this.post_content = data.post_content;

        this.post_content  = this.post_content.replace(/<p\s/g, '<p data-aos="fade-right" ');
        this.post_content  = this.post_content.replace(/<p>/g, '<p data-aos="fade-right">');
        this.post_content  = this.post_content.replace(/<div/g, '<div data-aos="fade-right" ');
        this.post_content  = this.post_content.replace(/<h1/g, '<h1 data-aos="fade-right" ');
        this.post_content  = this.post_content.replace(/<h2/g, '<h2 data-aos="fade-right" ');
        this.post_content  = this.post_content.replace(/<h3/g, '<h3 data-aos="fade-right" ');
        this.post_content  = this.post_content.replace(/<h4/g, '<h4 data-aos="fade-right" ');
        this.post_content  = this.post_content.replace(/<img/g, '<img data-aos="fade-right" ');
        this.post_content  = this.post_content.replace(/<pre\s/g, '<pre data-aos="fade-right" ');
        this.post_content  = this.post_content.replace(/<pre>/g, '<pre data-aos="fade-right">');
        
        this.show = true;
        AOS.init();
      } else {
        this.router.navigate(['404']);
      }
    })
  }

  ngOnInit() {   
      this.loadPage();
  }

}
